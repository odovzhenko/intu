from user import User


def before_all(context):
    context.user = User()


def after_all(context):
    context.user.driver.quit()
