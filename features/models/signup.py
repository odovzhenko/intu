from util import *

signup_frame = 'ds-if'
inputs = '//font[.="{0}"]/ancestor::div[1]//input'
birthday_span = '//span[@class="datetime-part"]/input'
radio_label = '//*[.="{0}"]/ancestor::div[1]//label[.="{1}"]'
sign_up_button = '//input[@value="Sign-up"]'
errortext = '//font[.="{0}"]/ancestor::div[1]//div[@class="errortext"]'
confirmation = '//h1[.="Thank you for entering"]'


def inside_signup_frame(func):
    def wrapper(*args):
        iframe = args[0].find('class', signup_frame)
        args[0].driver.switch_to.frame(iframe)
        func(*args)
        args[0].driver.switch_to.default_content()
    return wrapper


class Signup(Util):
    def open_signup_page(self):
        self.driver.get("https://intu.co.uk/signup")
        self.driver.maximize_window()

    @inside_signup_frame
    def enter_value_in_input(self, value, field):
        self.fill_in("xpath", inputs.format(field.capitalize()), value)

    @inside_signup_frame
    def enter_birthday(self, value):
        birthday_spans = self.find_elements('xpath', birthday_span)
        value = value.split('/')
        for i in range(3):
            birthday_spans[i].send_keys(value[i])

    @inside_signup_frame
    def select_radio(self, value, select):
        self.click('xpath', radio_label.format(select.capitalize(), value))

    @inside_signup_frame
    def click_signup(self):
        self.click('xpath', sign_up_button)

    @inside_signup_frame
    def check_errortext(self, area, text):
        self.should_contain_text('xpath', errortext.format(area.capitalize()), text)

    def check_confirmation_page(self):
        self.find("xpath", confirmation)
        if self.driver.current_url != "https://em.intu.co.uk/p/4LV8-2SF/confirmation":
            raise Exception("You're not on confirmation page")

    def check_redirection(self):
        self.should_math_url("https://intu.co.uk/")
