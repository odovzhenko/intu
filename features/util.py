import time
from time import sleep

from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class Util(object):
    def __init__(self, driver):
        self.driver = driver

    def find(self, kind, locator, timeout=15, repeat=0):
        try:
            if kind == "id":
                WebDriverWait(self.driver, timeout).until(
                    lambda driver: driver.find_element_by_id(locator).is_displayed())
                WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable((By.ID, locator)))
                return self.driver.find_element_by_id(locator)
            elif kind == "class":
                WebDriverWait(self.driver, timeout).until(
                    lambda driver: driver.find_element_by_class_name(locator).is_displayed())
                WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable((By.CLASS_NAME, locator)))
                return self.driver.find_element_by_class_name(locator)
            elif kind == "xpath":
                WebDriverWait(self.driver, timeout).until(
                    lambda driver: driver.find_element_by_xpath(locator).is_displayed())
                WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable((By.XPATH, locator)))
                return self.driver.find_element_by_xpath(locator)
        except (StaleElementReferenceException, ElementNotVisibleException, NoSuchElementException) as e:
            if repeat < 4:
                time.sleep(0.5)
                self.find(kind, locator, 1, repeat + 1)
            else:
                raise e
        except Exception as e:
            raise e

    def find_elements(self, kind, locator, repeat=0):
        try:
            if kind == "id":
                return self.driver.find_elements_by_id(locator)
            elif kind == "class":
                return self.driver.find_elements_by_class_name(locator)
            elif kind == "xpath":
                return self.driver.find_elements_by_xpath(locator)
        except (StaleElementReferenceException, ElementNotVisibleException, NoSuchElementException) as e:
            if repeat < 4:
                time.sleep(0.5)
                self.find_elements(kind, locator, repeat + 1)
            else:
                raise e
        except Exception as e:
            raise e

    def fill_in(self, kind, locator, text, repeat=0):
        try:
            self.find(kind, locator).send_keys(text)
        except TimeoutException as e:
            raise e
        except Exception as e:
            if repeat < 4:
                sleep(0.5)
                self.fill_in(kind, locator, text, repeat + 1)
            else:
                raise e

    def click(self, kind, locator, timeout=15, repeat=0):
        try:
            self.find(kind, locator, timeout).click()
        except TimeoutException as e:
            raise e
        except Exception as e:
            if repeat < 4:
                sleep(0.5)
                self.click(kind, locator, timeout, repeat + 1)
            else:
                raise e

    def clear(self, kind, locator, repeat=0):
        try:
            self.find(kind, locator).clear()
        except TimeoutException as e:
            raise e
        except Exception as e:
            if repeat < 4:
                sleep(0.5)
                self.clear(kind, locator, repeat + 1)
            else:
                raise e

    def should_contain_text(self, kind, locator, text, timeout=5):
        try:
            if kind == "id":
                WebDriverWait(self.driver, timeout).until(EC.text_to_be_present_in_element((By.ID, locator), text))
            elif kind == "class":
                WebDriverWait(self.driver, timeout).until(
                    EC.text_to_be_present_in_element((By.CLASS_NAME, locator), text))
            elif kind == "xpath":
                WebDriverWait(self.driver, timeout).until(EC.text_to_be_present_in_element((By.XPATH, locator), text))
        except Exception as e:
            raise e

    def should_math_url(self, url, timeout=10):
        try:
            WebDriverWait(self.driver, timeout).until(EC.url_to_be(url))
        except Exception as e:
            raise e
