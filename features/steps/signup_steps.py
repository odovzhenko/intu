from behave import given, then, when


@given('open sign up page')
def open_signup_page(context):
    context.user.open_signup_page()


@then('user enters {value} to {field} field')
def enter_data(context, value, field):
    context.user.enter_value_in_input(value, field)


@then('user enters {value} as birthday')
def enter_birthday(context, value):
    context.user.enter_birthday(value)


@then('user selects {value} in {select}')
def select_radio(context, value, select):
    context.user.select_radio(value, select)


@when('user clicks on sing-up button')
def click_signup(context):
    context.user.click_signup()


@then('user checks {text} error in {area} block')
def select_radio(context, text, area):
    context.user.check_errortext(area, text)


@then('user should sees confirmation page')
def check_confirmation_page(context):
    context.user.check_confirmation_page()


@then('user should be redirected to main page')
def check_redirection(context):
    context.user.check_redirection()
