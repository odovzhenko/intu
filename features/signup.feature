Feature: Sign up
  As a user I want to sign up

  @signup
  Scenario: Check error if first name is empty
    Given open sign up page
    Then user enters   to first name field
    Then user enters Black to last name field
    Then user enters joe.black@gmail.com to email address field
    Then user enters 12/12/1988 as birthday
    Then user selects Male in gender
    Then user enters B4 7DA to postcode field
    Then user selects Yes in Stay in touch
    When user clicks on sing-up button
    Then user checks Please enter your first name error in first name block

  @signup
  Scenario: Check error if invalid email address is used
    Given open sign up page
    Then user enters Joe to first name field
    Then user enters Black to last name field
    Then user enters joe.black to email address field
    Then user enters 12/12/1988 as birthday
    Then user selects Male in gender
    Then user enters B4 7DA to postcode field
    Then user selects Yes in Stay in touch
    When user clicks on sing-up button
    Then user checks Please enter correct email address error in email address block

  @signup
  Scenario: Sign up with valid data
    Given open sign up page
    Then user enters Joe  to first name field
    Then user enters Black to last name field
    Then user enters joe.black@gmail.com to email address field
    Then user enters 12/12/1988 as birthday
    Then user selects Male in gender
    Then user enters B4 7DA to postcode field
    Then user selects Yes in Stay in touch
    When user clicks on sing-up button
    Then user should sees confirmation page
    Then user should be redirected to main page
