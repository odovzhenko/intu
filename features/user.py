from models.signup import Signup
from selenium import webdriver


class User(Signup):
    def __init__(self):
        self.driver = webdriver.Chrome()
        if not self.driver:
            raise Exception("Unable to start driver")
        super(self.__class__, self).__init__(self.driver)
